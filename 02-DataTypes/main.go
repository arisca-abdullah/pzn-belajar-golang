package main

import "fmt"

func main() {
	// Number
	fmt.Println(1)
	fmt.Println(1.1)

	// String
	fmt.Println("Hello")
	fmt.Println(`World`)

	// Boolean
	fmt.Println(true)
	fmt.Println(false)
}