package main

import "fmt"

func main() {
	// Arithmetic Operator
	fmt.Println(5 + 5)
	fmt.Println(10 - 5)
	fmt.Println(5 * 4)
	fmt.Println(20 / 4)
	fmt.Println(20.0 / 8)
	fmt.Println(10 % 4)

	// Logical Operator
		// AND
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(false && true)
	fmt.Println(false && false)
		// OR
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(false || true)
	fmt.Println(false || false)
		// NOT
	fmt.Println(!true)
	fmt.Println(!false)
}