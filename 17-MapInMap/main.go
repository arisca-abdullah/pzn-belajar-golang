package main

import "fmt"

func main() {
	address := map[string]string {
		"state": "Indonesia",
		"city": "Purbalingga",
	}
	
	fmt.Println(address)

	peoples := map[string]map[string]string {
		"John Doe": map[string]string {
			"state": "UK",
			"city": "London",
		},
		"Jane Doe": map[string]string {
			"state": "US",
			"city": "New York",
		},
		"Jun Doe": map[string]string {
			"state": "Japan",
			"city": "Tokyo",
		},
	}

	for people, address := range(peoples) {
		fmt.Println(people, ":", address)
	}
	
	fmt.Println("=======================================")
	
	delete(peoples, "Jun Doe")
	
	for people, address := range(peoples) {
		fmt.Println(people, ":", address)
	}
}