package main

import "fmt"

func main() {
	address := make(map[string]string)

	address["state"] = "Indonesia"
	address["region"] = "Central Java"
	address["city"] = "Purbalingga"
	address["street"] = "Kopral Tanwir Street"

	fmt.Println(address)
	
	for key, value := range(address) {
		fmt.Println(key, ":", value)
	}
}