package main

import "fmt"

func main() {
	// Comparison Operators
	fmt.Println(10 == 10)
	fmt.Println(9 == 10)

	fmt.Println("======")
	
	fmt.Println(8 != 9)
	fmt.Println(9 != 9)
	
	fmt.Println("======")
	
	fmt.Println(5 < 8)
	fmt.Println(6 < 6)
	fmt.Println(9 < 7)
	
	fmt.Println("======")
	
	fmt.Println(6 > 7)
	fmt.Println(8 > 8)
	fmt.Println(7 > 4)
	
	fmt.Println("======")
	
	fmt.Println(3 >= 9)
	fmt.Println(4 >= 4)
	fmt.Println(5 >= 2)
	
	fmt.Println("======")
	
	fmt.Println(6 <= 7)
	fmt.Println(5 <= 5)
	fmt.Println(4 <= 1)
}