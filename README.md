# Belajar Golang

> _Belajar [Golang](https://www.youtube.com/playlist?list=PL-CtdCApEFH_t5_dtCQZgWJqWF45WRgZw) di Channel Youtube [Programmer Zaman Now](https://www.youtube.com/channel/UC14ZKB9XsDZbnHVmr4AmUpQ)_

## Topik

1. [Hello, World!](https://www.youtube.com/watch?v=_kmYSJeCZPE)
2. [Tipe Data](https://www.youtube.com/watch?v=rS83jDWAH5o)
3. [Operator Aritmatika dan Logika](https://www.youtube.com/watch?v=hiRw4Zvjyw4)
4. [String](https://www.youtube.com/watch?v=qGLHHNzPrvk)
5. [Variabel](https://www.youtube.com/watch?v=Y9ku97zaRYU)
6. [Konversi Tipe Data](https://www.youtube.com/watch?v=VI2zRhAav0U)
7. [Increment dan Decrement](https://www.youtube.com/watch?v=YHn1CCmz1qU)
8. [Operator Perbandingan](https://www.youtube.com/watch?v=2zt9XKRoOS4)
9. [Perulangan For](https://www.youtube.com/watch?v=9IH9Ap6sBKQ)
10. [Percabangan If](https://www.youtube.com/watch?v=xkWqVTRryEc)
11. [Percabangan Switch](https://www.youtube.com/watch?v=6RkA_HknWMg)
12. [Break dan Continue](https://www.youtube.com/watch?v=QLH9pWAK1ZQ)
13. [Array](https://www.youtube.com/watch?v=SoppAJ-cEVc)
14. [Slice](https://www.youtube.com/watch?v=_8S7H_nN81U)
15. [Map](https://www.youtube.com/watch?v=xQGKprwL6EY)
