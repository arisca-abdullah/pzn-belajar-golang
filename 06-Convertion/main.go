package main

import "fmt"
import "strconv"

func main() {
	// Int to Float
	var age int32 = 33
	var ageFloat float64 = float64(age)
	fmt.Println(ageFloat)
	
	// Float to Int
	var height float64 = 185.8
	var heightInt int32 = int32(height)
	fmt.Println(heightInt)

	// String to Integer
	stock, _ := strconv.Atoi("300")
	fmt.Println(stock)
	
	// Integer to String
	price := strconv.Itoa(20000)
	fmt.Println(price)
	
	// String to Boolean
	b, _ := strconv.ParseBool("true")
	fmt.Println(b)
	
	// String to Float
	f, _ := strconv.ParseFloat("3.14", 64)
	fmt.Println(f)
	
	// String to Integer
	i, _ := strconv.ParseInt("-32", 10, 64)
	fmt.Println(i)
	
	// String to Unsigned Integer
	u, _ := strconv.ParseUint("32", 10, 64)
	fmt.Println(u)
	
	// Boolean to String
	sfb := strconv.FormatBool(false)
	fmt.Println(sfb)
	
	// Float to String
	sff := strconv.FormatFloat(3.14, 'E', -1, 64)
	fmt.Println(sff)
	
	// Integer to String
	sfi := strconv.FormatInt(-36, 16)
	fmt.Println(sfi)
	
	// Unsigned Integer to String
	sfu := strconv.FormatUint(36, 16)
	fmt.Println(sfu)
}
