package main

import "fmt"
import "runtime"

func main() {
	for i := 1; i <= 10; i++ {
		
		switch i {
		case 1:
			fmt.Println("One")
		case 2:
			fmt.Println("Two")
		case 3:
			fmt.Println("Three")
		case 4:
			fmt.Println("Four")
		case 5:
			fmt.Println("Five")
		default:
			fmt.Println("Unknown")
		}
		
	}
	
	OS := runtime.GOOS
	
	switch OS {
	case "darwin":
		fmt.Println("Mac")
	case "linux":
		fmt.Println("Linux")
	case "windows":
		fmt.Println("Windows")
	default:
		fmt.Println("Unknown")
	}
}