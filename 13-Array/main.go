package main

import "fmt"

func main() {
	
	var names [3]string

	names[0] = "John Doe"
	names[1] = "Jane Doe"
	names[2] = "Jun Doe"

	fmt.Println(names)
	
	for i := 0; i < len(names); i++ {
		fmt.Println(names[i])
	}
	
	for	_, name := range(names) {
		fmt.Println(name)
	}

}