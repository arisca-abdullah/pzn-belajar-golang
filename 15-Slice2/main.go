package main

import "fmt"

func main() {
	slice1 := []string{
		"John Doe",
		"Jane Doe",
		"Jun Doe",
	}
	
	fmt.Println(slice1)
	
	fmt.Println("=====================")
	
	slice2 := make([]string, 3)
	slice2[0] = "Java"
	slice2[1] = "Python"
	slice2[2] = "Ruby"
	
	fmt.Println(slice2)
	
	slice3 := append(slice2, "Go", "Dart")
	
	fmt.Println(slice3)
	
	slice3[0] = "Javascript"
	
	fmt.Println(slice2)
	fmt.Println(slice3)
	
	fmt.Println("=====================")

	slice4 := make([]string, 3)
	copy(slice4, slice2)
	
	fmt.Println(slice2)
	fmt.Println(slice4)
	
	slice4[0] = "Swift"
	
	fmt.Println(slice2)
	fmt.Println(slice4)
}