package main

import "fmt"

func main() {
	languages := [5]string{
		"Javascript",
		"Python",
		"C++",
		"Dart",
		"Go",
	}

	fmt.Println(languages)
	
	slice1 := languages[1:4]
	slice2 := languages[:3]
	slice3 := languages[2:]

	fmt.Println(slice1)
	fmt.Println(slice2)
	fmt.Println(slice3)

	languages[1] = "Java"
	slice1[1] = "C#"

	fmt.Println(languages)
	fmt.Println(slice1)
	fmt.Println(slice2)
	fmt.Println(slice3)
}