package main

import "fmt"

func main() {
	fmt.Println("Learn Golang")
	fmt.Println("Learn" + " " + "Golang")
	fmt.Println(len("Golang"))
	fmt.Println("Golang"[0])
	fmt.Println("Golang"[1:4])
	fmt.Println("Golang"[2:])
	fmt.Println("Golang"[:2])
}