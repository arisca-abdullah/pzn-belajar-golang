package main

import "fmt"

// Global Variable
var global string = "Global Variable"

func main() {	
	fmt.Println(global)

	// Initialization
	var hello string = "Hello, World!"
	fmt.Println(hello)
	
	hello = "Hello, Golang!"
	fmt.Println(hello)
	
	// Declaration
	var name string
	fmt.Println(name)
	
	name = "John Doe"
	fmt.Println(name)
	
	// Shorter statement
	country := "Indonesia"
	fmt.Println(country)
	
	// Constant
	const PI = 3.14
	fmt.Println(PI)
}