package main

import "fmt"

func main() {
	i := 1

	// Increment
	// i = i + 1
	i++

	fmt.Println(i)
	
	// Decrement
	// i = i -1
	i--
	fmt.Println(i)
}